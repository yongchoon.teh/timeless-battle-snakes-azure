using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starter.Api;
using Starter.Api.Requests;
using Starter.Api.Responses;

namespace BattleSnakeInAzureFunction
{
    public static class move
    {

        /// <summary>
        /// This request will be sent for every turn of the game.
        /// Use the information provided to determine how your
        /// Battlesnake will move on that turn, either up, down, left, or right.
        /// </summary>
        //[FunctionName("move")]
        //public static async Task<IActionResult> Run(
        //    [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs/move")] HttpRequest req,
        //    ILogger log)
        //{
        //    log.LogInformation("C# HTTP trigger function processed a request.");

        //    string name = req.Query["name"];

        //    string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
        //    GameStatusRequest gameStatusRequest = JsonConvert.DeserializeObject<GameStatusRequest>(requestBody);


        //    //version 1, eat food only, without any forcast
        //    var direction = new List<string> { "down", "left", "right", "up" };

        //    var snakeMove = "down";

        //    var moveResponse = new MoveResponse
        //    {
        //        Move = snakeMove,
        //        Shout = "I am moving!"
        //    };

        //    return new OkObjectResult(moveResponse);
        //}

        //[FunctionName("move")]
        //public static async Task<IActionResult> Run(
        //    [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs/move")] HttpRequest req,
        //    ILogger log)
        //{
        //    log.LogInformation("C# HTTP trigger function processed a request.");

        //    string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
        //    GameStatusRequest gameStatusRequest = JsonConvert.DeserializeObject<GameStatusRequest>(requestBody);

        //    var snakeMove = GetNextMove(gameStatusRequest);

        //    var moveResponse = new MoveResponse
        //    {
        //        Move = snakeMove,
        //        Shout = $"I am moving {snakeMove}!"
        //    };

        //    return new OkObjectResult(moveResponse);
        //}

        [FunctionName("move")]
        public static async Task<IActionResult> Run(
    [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs/move")] HttpRequest req,
    ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            GameStatusRequest gameStatusRequest = JsonConvert.DeserializeObject<GameStatusRequest>(requestBody);

            var mySnake = gameStatusRequest.You;
            var board = gameStatusRequest.Board;

            // Find the nearest food
            var nearestFood = board.Food.OrderBy(food => GetManhattanDistance(mySnake.Head, food)).FirstOrDefault();

            // Move towards the nearest food
            var snakeMove = GetMoveTowardsFood(mySnake.Head, nearestFood);

            var moveResponse = new MoveResponse
            {
                Move = snakeMove,
                Shout = "I am moving towards food!"
            };

            return new OkObjectResult(moveResponse);
        }

        private static string GetMoveTowardsFood(Coordinate head, Coordinate food)
        {
            if (head.X < food.X)
            {
                return "right";
            }

            if (head.X > food.X)
            {
                return "left";
            }

            if (head.Y < food.Y)
            {
                return "up";
            }

            if (head.Y > food.Y)
            {
                return "down";
            }

            return "up"; // Default move
        }

        private static int GetManhattanDistance(Coordinate a, Coordinate b)
        {
            return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
        }

        private static string GetNextMove(GameStatusRequest gameStatusRequest)
        {
            var head = gameStatusRequest.You.Head;
            var food = gameStatusRequest.Board.Food.OrderBy(
                f => Math.Abs(head.X - f.X) + Math.Abs(head.Y - f.Y)).FirstOrDefault();

            if (food == null)
            {
                return "up"; // Default move if no food is found
            }

            var possibleMoves = new List<string>();

            if (food.X < head.X)
            {
                possibleMoves.Add("left");
            }
            else if (food.X > head.X)
            {
                possibleMoves.Add("right");
            }

            if (food.Y < head.Y)
            {
                possibleMoves.Add("up");
            }
            else if (food.Y > head.Y)
            {
                possibleMoves.Add("down");
            }

            // Randomly select one of the possible moves
            var random = new Random();
            var direction = possibleMoves[random.Next(possibleMoves.Count)];

            return direction;
        }

    }
}
